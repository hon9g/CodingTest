'''
Today is January 1, 2007 Monday. So, what day of week on 2007.X.Y ? Write a program to find out.

'''

thirtyOne = [1,3,5,7,8,10,12]
thirty = [4,6,9,11]
week = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"]
M, days = map(int, input().split())
i = 1

while i != M:

    if i in thirtyOne:
        days += 31
    if i in thirty:
        days += 30
    if i == 2:
        days += 28

    i += 1


print(week[days%7])